%parpool(20)
clc
clear
 load bigall.mat
 load disdept.mat
 load undum.mat
 load lists.mat
 load sco.mat
 load ot.mat
%%
dis2=dis./1000;
N=max(size(alldata));
J=max(size(allxi));
%%
run percmake.m  %for "on list" and "s-hat<c<s" dummies
%%

%%%EXCLUDE THOSE WITH MISSING VARIABLES
    
% male1=[];
% female1=[];
% %to seperate genders
% 
% for i=1:N
%      if alldata(i,1)==1 && isnan(alldata(i,9))==0 && sum(dis(i,:))~=0
%         male1=[male1; i];
%     elseif alldata(i,1)==0 && isnan(alldata(i,9))==0 && sum(dis(i,:))~=0
%         female1=[female1; i];
%     end
% end

all=[];
for i=1:N
    if isnan(alldata(i,9))==0 && sum(dis(i,:))~=0  
        all=[all;i];
    end
end
% male=[];
% female=[];
% NN=max(size(all));
% for i=1:NN
%     if alldata(all(i),1)==1 && isnan(alldata(all(i),9))==0 && sum(dis(all(i),:))~=0
%         male=[male; i];
%     elseif alldata(all(i),1)==0 && isnan(alldata(all(i),9))==0 && sum(dis(all(i),:))~=0
%         female=[female; i];
%     end
% end
%%
X={};
NN=max(size(all));

parfor i=1:NN
    X{i}=[];
   
            
    X{i}(:,:)=[allxi(:,1:27) alldata(all(i),1).*allxi(:,1:27) dis(all(i),:)' alldata(all(i),1).*dis(all(i),:)' dis2(all(i),:)'.*allxi(:,5) alldata(all(i),1).*dis2(all(i),:)'.*allxi(:,5) dis2(all(i),:)'.*allxi(:,2) alldata(all(i),1).*dis2(all(i),:)'.*allxi(:,2) d(all(i),:)' alldata(all(i),1).*d(all(i),:)' d2(all(i),:)' alldata(all(i),1).*d2(all(i),:)' d(all(i),:)'.*d2(all(i),:)' alldata(all(i),1).*d(all(i),:)'.*d2(all(i),:)'  ef(all(i))*d(all(i),:)'.*d2(all(i),:)' alldata(all(i),1).*ef(all(i))*d(all(i),:)'.*d2(all(i),:)' zeros(J,1) zeros(J,1)];
          %X{i}(j,:)=[allxi(j,1:27) dis(i,j) dis2(i,j)*allxi(j,5) dis2(i,j)*allxi(j,2) alldata(i,9)*allxi(j,2) ];
     
    X{i}(J+1,:)=[zeros(1,68) alldata(all(i),1) 1];
    
end






%%
%y records where each student is placed
y=[];
for i=1:N
    if ismember(codepl(i),allcode(:,1))==1 
        y(i)=max(find(allcode(:,1)==codepl(i)));
    else
        y(i)=2462; %outside option
    end
end

%%
%1x(J+1) for each student, 1 if placed 0 otherwise
yn=zeros(N,J+1);
for i=1:N
    yn(i,y(i))=1;
end

%%
%finding what's available for each student
%For some students, school they are placed is not available for them.

alldata(:,5)=sco(:,1);
alldata(:,6)=sco(:,2);

A={};
for i=1:N
    A{i}=[];
    for j=1:J
        %if studmedtype(i,4)>=medcodes(j,2)
       if j~=955 && j~=956 && j~=1072 && j~=1073 && j~=1074 && j~=1075 && j~=1208 && j~=1209 && j~=2009 && j~=2010 
           if field(j)==1
             if alldata(i,5)>=allcode(j,2)
                 A{i}=[A{i}; j allcode(j,1)];
             end
           else
              if alldata(i,6)>=allcode(j,2)
                 A{i}=[A{i}; j allcode(j,1)];
              end
           end
        end    
     end
 end



for i=1:N
    if alldata(i,1)==1
        if alldata(i,6)>=allcode(955,2)
            A{i}=[A{i}; 955 allcode(955,1)];
        end
        if alldata(i,5)>=allcode(1072,2)
            A{i}=[A{i}; 1072 allcode(1072,1)];
        end
        if alldata(i,5)>=allcode(1075,2)
            A{i}=[A{i}; 1075 allcode(1075,1)];
        end
        if alldata(i,5)>=allcode(1209,2)
            A{i}=[A{i}; 1209 allcode(1209,1)];
        end
        if alldata(i,5)>=allcode(2009,2)
            A{i}=[A{i}; 2009 allcode(2009,1)];
        end
      
    end
end

for i=1:N
    if alldata(i,1)==0
        if alldata(i,6)>=allcode(956,2)
            A{i}=[A{i}; 956 allcode(956,1)];
        end
        if alldata(i,5)>=allcode(1073,2)
            A{i}=[A{i}; 1073 allcode(1073,1)];
        end
        if alldata(i,5)>=allcode(1074,2)
            A{i}=[A{i}; 1074 allcode(1074,1)];
        end
        if alldata(i,5)>=allcode(1208,2)
            A{i}=[A{i}; 1208 allcode(1208,1)];
        end
        if alldata(i,5)>=allcode(2010,2)
            A{i}=[A{i}; 2010 allcode(2010,1)];
        end
    end
end


for i=1:N
    if ismember(y(i), A{i})==0 && y(i)~=J+1
        A{i}=[A{i}; y(i) allcode(y(i),1)];
    end
end

for i=1:N
    A{i}=[A{i};J+1 0];
end






%%

 run estall3.m
 save('outa.mat','of3')
 save('eflag.mat','exitflag')



