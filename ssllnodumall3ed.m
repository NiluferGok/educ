function [L,GG] = ssllnodumall3ed(beta,xn,yn,A,gi)
  %X IS JUST X(ALL)


%gi is the gender index matrix
%g=1 if if male, 0 is female
N=max(size(gi)); %number of students of that gender
lin=zeros(N,1);% numerator
J=min(size(yn)); %number of programs including the outside option
lid=zeros(N,1); %denominator
%sumin=zeros(N,1);
G=zeros(max(size(beta)),N);

parfor i=1:N
 %version 2
    
 
 lin(i)=beta*xn{i}'*yn(i,:)';
 
 lid(i)=(ones(1,J)*log(sum(exp(beta*xn{i}(A{i}(:,1),:)'))))*yn(i,:)';  
 %version 1   
%  cn=xn{i}'; 
%  vn=beta*cn;
%  lin(i)=vn*yn(i,:)';
%  cn2=xn{i}(A{i}(:,1),:)';
%  sumin=sum(exp(beta*cn2));
%  lid(i)=(ones(1,J)*log(sumin))*yn(i,:)';
 %for gradient
 
  Pn=exp(beta*xn{i}')./sum(exp(beta*xn{i}(A{i}(:,1),:)'));
  
  G(:,i)=(-(yn(i,A{i}(:,1))-Pn(A{i}(:,1)))*xn{i}(A{i}(:,1),:));
end
toc
%size(sum(G))
% for ii=1:N
%  i=gi(ii);
%  cn=xn{i}(A(:,1),:)';
%  
%  sumin(ii)=sum(exp(beta*cn));
% %  
% %  
% %  
% %  
% %  k=size(A{i},1)-1;
% %  cn=xn{i}';
% %  for j=1:k
% %      sumin(ii)=sumin(ii)+exp(beta(1:106)*cn(:,A{i}(j,1)));
% %  end
% %  %sumin(ii)=sumin(ii)+exp(beta(9)+gamma(J)*e(i));
% %   sumin(ii)=sumin(ii)+exp(beta(107));
% end




% for i=1:N
%     ii=gi(i);
%     for j=1:J
%         lid(i)=lid(i)+yn(ii,j)*log(sumin(i));
%     end
% end


% L=(sum(lin)-sum(lid));
L=-(sum(lin)-sum(lid));
disp(['NARGOUT=' num2str(nargout)])
if nargout>1
    GG=sum(G,2);
end
      
end
