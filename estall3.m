beta0=ones(1,70)*-0.00001;
ub=[ones(1,69)*inf 0 ];
lb=[ones(1,69)*-inf 0 ];

options = optimoptions('fmincon'); 
options.StepTolerance =1e-12;
options.Display = 'iter';
options.MaxFunEvals=40500;
option.UseParallel='true';
t0=ot;
options.GradObj='on';
options.SpecifyObjectiveGradient= true;
tic

[of3,fval,exitflag]= fmincon(@(thet) ssllnodumall3ed(thet(1:70),X,yn(all,:),{A{all}},all),t0,[],[],[],[],lb,ub,[],options);
toc